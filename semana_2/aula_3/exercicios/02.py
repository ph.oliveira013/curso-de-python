def soma(x, y):
    return x + y


assert soma(1, 1) == 2
assert soma(1, 2) == 3
assert soma(0, 0) == 0
assert soma(7, 7) == 14
