from unittest import TestCase

class Calc:
  def add(self, x, y):
    return x + y

  def sub(self, x, y):
    return x + y

  def mult(self, x, y):
    return x + y

  def div(self, x, y):
    return x + y


class CalcTests(TestCase):
    def test_soma_deve_retornar_2_com_entrada_de_1_1(self):
        c = Calc()

        # Dado o valor de entrada 1, 1
        entrada = c.add(1, 1)

        # Então o valor obtido deve ser 2
        resultado_esperado = 2

        self.assertEqual(entrada, resultado_esperado)
