from unittest import TestCase

class Calc:
  def add(self, x, y):
    return x + y

  def sub(self, x, y):
    return x - y

  def mult(self, x, y):
    return x * y

  def div(self, x, y):
    return x + y


class CalcSomaTests(TestCase):
    def test_add_deve_retornar_2_com_entrada_de_1_1(self):
        c = Calc()

        # Dado o valor de entrada 1, 1
        entrada = c.add(1, 1)

        # Então o valor obtido deve ser 2
        resultado_esperado = 2

        self.assertEqual(entrada, resultado_esperado)


class CalcSubTests(TestCase):
    def test_sub_deve_retornar_0_com_entrada_de_1_1(self):
        c = Calc()

        # Dado o valor de entrada 1, 1
        entrada = c.sub(1, 1)

        # Então o valor obtido deve ser 0
        resultado_esperado = 0

        self.assertEqual(entrada, resultado_esperado)


class CalcMultTests(TestCase):
    def test_mult_deve_retornar_10_com_entrada_de_2_5(self):
        c = Calc()

        # Dado o valor de entrada 2, 5
        entrada = c.mult(2, 5)

        # Então o valor obtido deve ser 10
        resultado_esperado = 10

        self.assertEqual(entrada, resultado_esperado)

    ...
