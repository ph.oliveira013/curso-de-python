from unittest import TestCase

from collections import Counter


def conta_letras(texto):
    return Counter(xpto)


class TestContaLetras(TestCase):
    def test_conta_letras_recebe_hello_e_retorna_sua_contagem(self):
        entrada = 'hello'
        esperado = {'h': 1, 'e': 1, 'l': 2, 'o': 1}
        self.assertEqual(conta_letras(entrada), esperado)

    def test_conta_letras_recebe___e_retorna_sua_contagem(self):
        entrada = ''
        esperado = {}
        self.assertEqual(conta_letras(entrada), esperado)

    def test_conta_letras_recebe_espaco_e_retorna_sua_contagem(self):
        entrada = ' '
        esperado = {' ': 1}
        self.assertEqual(conta_letras(entrada), esperado)

    def test_conta_letras_recebe_lista_e_retorna_sua_contagem(self):
        entrada = ['hello']
        esperado = {'h': 1, 'e': 1, 'l': 2, 'o': 1}
        self.assertEqual(conta_letras(entrada), esperado)
