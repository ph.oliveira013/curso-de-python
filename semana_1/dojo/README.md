# Dojo semana 1

Nos paths se encontram os resultados feitos em aula

## problema 1
Complete a solução para que ela inverta todas as palavras dentro da string passada.

Exemplo:
```
reverse_worlds("A maior vitória é aquela que não requer batalha")
# 'batalha requer não que aquela é vitória maior A'
```

## problema 2

Crie uma função is_divisible(n, x, y) que verifica se um número n é divisível por dois números x e y.
Todas as entradas são dígitos positivos, diferentes de zero.

```
Exemplos:
is_divisible(3,1,3)--> True
is_divisible(12,2,6)--> True
is_divisible(100,5,3)--> False
is_divisible(12,7,5)--> False
```

## problema 3

Pegue duas strings s1 e s2 e com elas monte uma nova string contendo todos os caracteres das duas strings.

Regras:
  - Não deve haver chars repetidos
  - Eles devem estar em ordem alfabética

Exemplos:

```
a = "xyaabbbccccdefww"
b = "xxxxyyyyabklmopq"
longest(a, b) -> "abcdefklmopqwxy"

a = "abcdefghijklmnopqrstuvwxyz"
longest(a, a) -> "abcdefghijklmnopqrstuvwxyz"
```

## problema 4
Dado um dicionário de linguagens e seus respectivos resultados de teste, retorne a lista das linguagens onde sua pontuação de teste é pelo menos 60, em ordem decrescente dos resultados.

Nota: Não haverá valores duplicados.


```
{"Java": 10, "Ruby": 80, "Python": 65}  --> ["Ruby", "Python"]
{"Hindi": 60, "Dutch" : 93, "Greek": 71} --> ["Dutch", "Greek", "Hindi"]
{"C++": 50, "ASM": 10, "Haskell": 20}   --> []
```
