# def my_languages(dicionario):
#     lista = []
#     for indice in dicionario:
#         if dicionario[indice] >= 60:
#             lista.append(indice)
#     return lista


def my_languages(dicionario):
    return [key for key, value in dicionario.items() if value >= 60]
