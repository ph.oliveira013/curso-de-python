numero_1 = int(input('Digite o primeiro numero: '))
numero_2 = int(input('Digite o segundo numero: '))
numero_real = float(input('Digite o numero Real: '))

resultado_1 = numero_1 * 2 * (numero_2 / 2)
print('o produto do dobro do primeiro com metade do segundo = {}'.format(resultado_1))

resultado_2 = numero_1 * 3 + numero_real
print('a soma do triplo do primeiro com o terceiro = {}'.format(resultado_2))

resultado_3 = numero_real ** 3
print('o terceiro elevado ao cubo = {}'.format(resultado_3))
