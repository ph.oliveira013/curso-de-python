## Problemas 1b

1. Qual o paradigma da sua linguagem preferida? Por que você acredita que ela se comporta dessa maneira?

2. Defina a abstração contida em uma função de soma:

```
Ex: f(x, y) -> x + y
```

3. Descreva a abstração de uma fila. Como ela se comporta?

4. Descreva a abstração do tipo Fila e seus processos.

5. Faça um programa que itere em uma lista de maneira imperativa e que armazene em uma nova lista seu valor processado por uma
função:

```
entrada = [‘foo’, ‘bar’, ‘spam’, ‘eggs’]
função = f(x) = x * 2
saida = [‘foofoo’, ‘barbar’ ‘spamspam’, ‘eggseggs’]
```

6. Usando os mesmos inputs do código anterior, reconstrua o problema de maneira declarativa.

```
Funções que pode te ajudar:
    - operator.mul
    - map()
```

7. Faça um programa que faça a inversão de uma lista usando as propriedade mutáveis dessa lista (remova da lista e insira de novo)

```
Entrada = [1, 2, 3]
Saída = [3, 2, 1]

Coisas que podem te ajudar:
list.insert, list.append, list.remove
```

8. Resolva de maneira imutável (Ou seja, retornando uma nova lista)

```
Entrada = [1, 2, 3]
Saída = [3, 2, 1]
Coisas que podem te ajudar:
list.insert, list.append, list.remove
```

9. Crie uma função que faça uma saudação a alguém. A função deve receber dois argumentos ‘saudação’ e ‘nome’.

```
f(‘Ahoy’, ‘Fausto’) -> ‘Ahoy Fausto’
f(‘Olá’, bb’) -> Olá bb’
```

10. Especifique uma função que receba um número uma string e retorne se na string há o mesmo número de elementos que foram passados no parâmetro.

```
f(3, ‘aaa’) -> True
f(10, ‘Batata’) -> False
```

11. Faça uma função que não tenho a cláusula de retorno e mostre seu tipo

12. Faça uma função que receba um número, caso esse número seja múltiplo de 3, retorne “queijo”, caso contrário, retorne o valor de entrada.

```
f(5) -> 5
f(3) -> ‘queijo’
f(6) -> ‘queijo’
```

13. Faça uma função que receba um número, caso esse número seja múltiplo de 5, retorne “goiabada”, caso contrário, retorne o valor de entrada.

```
f(5) -> ‘goiabada’
f(3) -> 3
f(10) -> ‘goiabada’
```

14. Faça uma função que receba um número, caso esse número seja múltiplo de 3 e 5, retorne “romeu e julieta”, caso contrário, retorne o valor de entrada.

```
f(3) -> 3
f(5) -> 5
f(15) -> ‘romeu e juleita’
```

15. Faça uma função que se aplique uma função duas vezes em um valor passado

```
reaplica(soma_2, 2) -> 6
reaplica(sub_2, 2) -> -2
```

16. Faça uma função que receba uma função e um número. Ela deve retornar uma lista com todos os números em ordem crescente até aquele valor:

```
aplica_em_lote(soma_1, 3) -> [1, 2, 3, 4]
DICA:
    O index inicia em 0
```

17. Faça uma função que receba uma função e uma lista de números, aplique essa função a todos os valores da lista e retorne uma nova lista com os valores processados:

```
aplica(func, list) -> [func(list[0]), func(list[n]), ..]
aplica(soma_1, [1, 2, 3]) -> [2, 3, 4]
aplica(sub_1, [1, 2, 3]) -> [0, 1, 2]
```

18. Usando as funções criadas nos exercícios 13, 14 e 15. Crie uma composição de funções que resolva os 3 casos:

```
EX:
f(x) -> Union[ID, goiabada, queijo, romeu e julieta]
f(3) -> ‘queijo’
f(5) -> ‘goiabada’
f(15) -> ‘romeu e julieta’
f(19) -> 19
```

19. Dada uma lista:
```
[‘keep’, ‘remove’, ‘keep’, ‘remove’, ‘keep’, ‘remove’]
```
Usando a função filter remova tudo que deve ser removido

20. Dada uma lista:
```
[‘keep’, ‘remove’, ‘keep’, ‘remove’, ‘keep’, ‘remove’]
```
Usando a função map transforme o primeiro carácter em maiúsculo.

21. Dada uma lista:
```
[‘keep’, ‘remove’, ‘keep’, ‘remove’, ‘keep’, ‘remove’]
```
Usando a função reduce faça a somatória de todos os elementos da lista juntos

```
[‘a’, ‘abc’, ‘def’] -> 7
```