"""
Faça uma função que receba um número, caso esse número seja múltiplo de 5,
    retorne “goiabada”, caso contrário, retorne o valor de entrada.
f(5) -> ‘goiabada’
f(3) -> 3
f(10) -> ‘goiabada’
"""


def f(n):
    return 'goiabada' if not n % 5 else n


assert f(3) == 3
assert f(5) == 'goiabada'
assert f(6) == 6
assert f(10) == 'goiabada'
