"""
Faça uma função que receba um número, caso esse número seja múltiplo de 3 e 5,
    retorne “romeu e julieta”, caso contrário, retorne o valor de entrada.
f(3) -> 3
f(5) -> 5
f(15) -> ‘romeu e juleita’
"""


def f(n):
    return 'romeu e julieta' if not n % 5 and not n % 3 else n


assert f(3) == 3
assert f(5) == 5
assert f(6) == 6
assert f(15) == 'romeu e julieta'
assert f(30) == 'romeu e julieta'
