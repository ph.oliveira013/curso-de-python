"""
Faça uma função que receba uma função e uma lista de números,
    aplique essa função a todos os valores da lista e
    retorne uma nova lista com os valores processados:

aplica(func, list) -> [func(list[0]), func(list[n]), ..]
aplica(soma_1, [1, 2, 3]) -> [2, 3, 4]
aplica(sub_1, [1, 2, 3]) -> [0, 1, 2]
"""


def aplica(func, list):
    l = []
    for el in list:
        l.append(func(el))
    return l


assert aplica(lambda x: x + 1, [1, 2, 3]) == [2, 3, 4]
assert aplica(lambda x: x - 1, [1, 2, 3]) == [0, 1, 2]
