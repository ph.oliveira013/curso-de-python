"""
Faça uma função que não tenho a cláusula de retorno e mostre seu tipo
"""

def f():
    ...


assert f() is None
