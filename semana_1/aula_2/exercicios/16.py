"""
Faça uma função que receba uma função e um número.

Ela deve retornar uma lista com todos os números
    em ordem crescente até aquele valor:

aplica_em_lote(soma_1, 3) -> [1, 2, 3, 4]

DICA:
    O index inicia em 0
"""


def aplica_em_lote(func, val):
    l = []
    for i in range(val):
        l.append(func(i))
    return l


assert aplica_em_lote(lambda x: x + 1, 3) == [1, 2, 3]
