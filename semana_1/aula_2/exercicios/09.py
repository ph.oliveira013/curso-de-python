"""
Crie uma função que faça uma saudação a alguém.
    A função deve receber dois argumentos ‘saudação’ e ‘nome’.
"""

def f(saudacao, nome):
    return f'{saudacao} {nome}'


assert f('Ahoy', 'Fausto') == 'Ahoy Fausto'
assert f('Olá', 'bb') == 'Olá bb'
