def read_file(file: str):
    try:
        return open(file)
    except FileNotFoundError:
        return 'Arquivo não existe'


print(read_file('isjaisa.txt'))
